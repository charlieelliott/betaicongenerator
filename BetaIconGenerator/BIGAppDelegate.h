//
//  BIGAppDelegate.h
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 10/10/13.
//  Copyright (c) 2013 Hidden World Hut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
