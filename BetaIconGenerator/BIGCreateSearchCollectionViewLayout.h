//
//  BIGCreateSearchCollectionViewLayout.h
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 2/20/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIGCreateSearchCollectionViewLayout : UICollectionViewLayout

@property (nonatomic) CGSize *itemSize;

@end
