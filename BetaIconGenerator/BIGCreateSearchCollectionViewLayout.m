////
////  BIGCreateSearchCollectionViewLayout.m
////  BetaIconGenerator
////
////  Created by Charlie Elliott on 2/20/14.
////  Copyright (c) 2014 Hidden World Hut. All rights reserved.
////
//
//#import "BIGCreateSearchCollectionViewLayout.h"
//
//@implementation BIGCreateSearchCollectionViewLayout
//
//- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
//{
//    NSMutableArray *attributesCollection = [[NSMutableArray alloc] init];
//    for(NSIndexPath *indexPath in self.visibleIndexPaths)
//    {
//        UICollectionViewLayoutAttributes *attributes = [self layoutAttributesForItemAtIndexPath:indexPath];
//        if(self.detailViewVisible)
//        {
//            attributes.alpha = 0;
//            [attributesCollection addObject:attributes];
//        }
//        else if(CGRectIntersectsRect(attributes.frame, self.layoutRect))
//            [attributesCollection addObject:attributes];
//    }
//    
//    for(int section=BTYChipsViewSectionGrid+1 ; section<self.numSections ; section++)
//    {
//        NSUInteger numItemsInSection = [self.numItemsPerSection[section] unsignedIntegerValue];
//        for(int item=0 ; item<numItemsInSection ; item++)
//            [attributesCollection addObject:[self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:item inSection:section]]];
//    }
//    
//    return attributesCollection;
//}
//
//- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    if(indexPath.section != BTYChipsViewSectionGrid)
//        return [super layoutAttributesForItemAtIndexPath:indexPath];
//    
//    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
//    attributes.size = self.itemSize;
//    CGRect frame = (CGRect){{0,0}, self.itemSize};
//    frame.origin.x = (self.itemSize.width * indexPath.row) + (self.minimumInterItemSpacing * indexPath.row) + self.sectionInsets.left;
//    frame.origin.y = COVER_FLOW_VERTICAL_OFFSET + self.sectionInsets.top;
//    attributes.frame = frame;
//    
//    CGFloat distance = CGRectGetMidX(self.visibleRect) - attributes.center.x;
//    CGFloat aDistance = [self activeDistance];
//    CGFloat mirror = (distance >= 0) ? 1 : -1;
//    CGFloat normalizedDistance = distance / aDistance;
//    CATransform3D transform = CATransform3DIdentity;
//    transform.m34 = -1 / self.perspectiveDistance;
//    
//    CGFloat rotation = MIN(self.maximumYAxisRotation * ABS(normalizedDistance * .55), self.maximumYAxisRotation);
//    transform = CATransform3DRotate(transform, rotation * mirror, 0, 1, 0);
//    
//    if(ABS(normalizedDistance) < 1)
//    {
//        CGFloat zoom = 1 + MIN(self.centerItemZoom * MAX(1 - ABS(distance / self.activeDistance), 0), self.centerItemZoom);
//        transform = CATransform3DScale(transform, zoom, zoom, 1.0);
//    }
//    else
//    {
//        CGFloat activeSecondaryDistance = (self.visibleRect.size.width / 2) - aDistance;
//        CGFloat adjustedDistance = ABS(distance) - aDistance;
//        CGFloat multiplier = (adjustedDistance / 2) / activeSecondaryDistance;
//        CGFloat constrainedMultiplier = MIN(multiplier, .4);
//        CGFloat offset = adjustedDistance * constrainedMultiplier;
//        transform = CATransform3DTranslate(transform, offset * mirror, 0, 0);
//        attributes.alpha = MAX(1 - (multiplier * 1.7), 0);
//    }
//    
//    attributes.zIndex = self.visibleRect.size.width-ABS(distance);
//    attributes.transform3D = transform;
//    
//    return attributes;
//}
//
//@end
