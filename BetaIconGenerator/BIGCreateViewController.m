//
//  BIGCreateViewController.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 2/20/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import "BIGCreateViewController.h"
#import "BIGIcon.h"
#import "BIGEffectsScene.h"

@interface BIGCreateViewController () <UITextFieldDelegate>
@property (nonatomic) IBOutlet UILabel *hashLabel;
@property (nonatomic) IBOutlet UITextField *searchField;
@property (nonatomic) IBOutlet UIImageView *artworkImageView;
@property (nonatomic) IBOutlet UISwitch *gridSwitch;

@property (nonatomic, strong) BIGIcon *currentIcon;
@property (nonatomic, strong) BIGEffectsScene *scene;

@property (nonatomic) UIView *snapShotButton;

@property (nonatomic) UIDynamicAnimator *animator;
@end

@implementation BIGCreateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.gridSwitch.on = NO;
    [self updateHashText:@""];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.searchField becomeFirstResponder];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

# pragma mark - Save
- (IBAction)saveButtonTouchDown:(UIButton *)sender
{
    self.snapShotButton = [sender snapshotViewAfterScreenUpdates:NO];
}

- (IBAction)saveButtonTouchUp:(UIButton *)sender
{
    [self.currentIcon writeIconBundleToDisk];
    
    [self.animator removeAllBehaviors];
    
    self.snapShotButton.center = sender.center;
    [self.view addSubview:self.snapShotButton];
    
    sender.alpha = 0.f;
    [UIView animateWithDuration:.6f delay:.3f options:UIViewAnimationOptionCurveEaseIn animations:^{
        sender.alpha = 1.f;
        self.snapShotButton.alpha = 0.f;
    } completion:nil];
    
    UIDynamicBehavior *labelBehavior = [UIDynamicBehavior new];
    UIGravityBehavior *grav = [[UIGravityBehavior alloc] initWithItems:@[self.snapShotButton]];
    grav.magnitude = 1.2f;
//    UIPushBehavior *push = [[UIPushBehavior alloc] initWithItems:@[snapshot] mode:UIPushBehaviorModeInstantaneous];
//    push.angle = M_PI;
//    push.magnitude = .2f;
    
    [labelBehavior addChildBehavior:grav];
//    [labelBehavior addChildBehavior:push];
    [self.animator addBehavior:labelBehavior];
}

# pragma mark - Grid Switch

- (IBAction)gridSwitchChanged:(id)sender
{
    [self updateHashText:self.currentIcon.inputString];
}

# pragma mark - Text

- (void)updateHashText:(NSString *)hash
{
    self.currentIcon = [BIGIcon iconFromString:hash showGrid:self.gridSwitch.on gridAlpha:.28f];
    self.artworkImageView.image = [self.currentIcon imageWithSize:self.artworkImageView.frame.size.width * [UIScreen mainScreen].scale mask:YES];
    
    NSString *rawHashString = [self.currentIcon hashString];
    NSMutableArray *array = [NSMutableArray new];
    for(int i = 0; i < [rawHashString length] - 1; i = i+2)
    {
        [array addObject:[rawHashString substringWithRange:NSMakeRange(i, 2)]];
    }
    
    NSRange firstRange = NSMakeRange(0, round([array count]/2));
    NSRange secondRange = NSMakeRange(firstRange.length, [array count] - firstRange.length);
    NSString *firstHalf = [[array subarrayWithRange:firstRange] componentsJoinedByString:@" "];
    NSString *secondHalf = [[array subarrayWithRange:secondRange] componentsJoinedByString:@" "];
    self.hashLabel.text = [NSString stringWithFormat:@"%@\n%@", firstHalf, secondHalf];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
//    [self.scene removeCurrentIconAtPosition:self.artworkImageView.center];
    [self updateHashText:text];
    
    return YES;
}

@end
