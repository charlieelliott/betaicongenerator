//
//  BIGEffectsScene.h
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 2/25/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

@import SpriteKit;

@interface BIGEffectsScene : SKScene

- (void)removeCurrentIconAtPosition:(CGPoint)point;

@end
