//
//  BIGEffectsScene.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 2/25/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import "BIGEffectsScene.h"

@implementation BIGEffectsScene

- (void)removeCurrentIconAtPosition:(CGPoint)point
{
    NSString *myParticlePath = [[NSBundle mainBundle] pathForResource:@"BIGIconExplosion" ofType:@"sks"];
    SKEmitterNode *snowParticle = [NSKeyedUnarchiver unarchiveObjectWithFile:myParticlePath];
    snowParticle.particlePosition = point;
    [self addChild:snowParticle];
}

@end
