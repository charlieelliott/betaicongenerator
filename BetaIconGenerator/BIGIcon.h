//
//  BIGIcon.h
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 10/10/13.
//  Copyright (c) 2013 Hidden World Hut. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTEncodableObject.h"

@interface BIGIcon : CTEncodableObject

@property (nonatomic, readonly) NSString *inputString;
@property (nonatomic, readonly) NSString *hashString;

@property (nonatomic, readonly) UIImage *previewImage;
@property (nonatomic, readonly) UIImage *artworkImage;
@property (nonatomic, readonly) UIImage *transparentArtworkImage;
@property (nonatomic, readonly) UIImage *glyphImage;

@property (nonatomic, assign) BOOL gridHidden;
@property (nonatomic, assign) CGFloat gridAlpha;

+ (instancetype)iconFromString:(NSString *)string showGrid:(BOOL)grid;
+ (instancetype)iconFromString:(NSString *)string showGrid:(BOOL)grid gridAlpha:(CGFloat)gridAlpha;

+ (instancetype)glyphIconFromString:(NSString *)string;

- (UIImage *)imageWithSize:(CGFloat)size mask:(BOOL)mask;

- (void)writeIconBundleToDisk;

//+ (BOOL)testOutputWithInput:(NSString *)inputString;

@end
