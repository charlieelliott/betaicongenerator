//
//  BIGIcon.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 10/10/13.
//  Copyright (c) 2013 Hidden World Hut. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "BIGIcon.h"
#import "UIColor+HexColor.h"
#import "NSData+SHA256.h"
#import "UIImage+Resize.h"

NS_INLINE CGPoint pt(CGFloat x, CGFloat y) { return CGPointMake(x, y); }

NS_INLINE void addLineFromStartPointToPoint(UIBezierPath *path, CGPoint start, CGPoint end)
{
    [path moveToPoint:start];
    [path addLineToPoint:end];
    [path closePath];
}

NS_INLINE void addCircleWithCenterAndRadius(UIBezierPath *path, CGPoint center, CGFloat radius)
{
    [path moveToPoint:center];
    [path addArcWithCenter:center radius:radius startAngle:-M_PI endAngle:M_PI clockwise:YES];
    [path closePath];
}

NS_INLINE void fillPathWithColorInContext(UIBezierPath *path, UIColor *color, CGContextRef context)
{
    CGContextSetFillColorWithColor(context, color.CGColor);
    [path fillWithBlendMode:kCGBlendModeNormal alpha:1.f];
}

NS_INLINE void fillRectangleWithColorInContext(CGRect rect, UIColor *color, CGContextRef context)
{
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:rect];
    fillPathWithColorInContext(path, color, context);
}

static CGFloat const iconScale = 2.f;
static CGFloat const iconSizeSettings = 29.f;
static CGFloat const iconSizeSpotlight = 40.f;
static CGFloat const iconSizeIphone = 60.f;
static CGFloat const iconSizeIpad = 76.f;
static CGFloat const iconSizeItunesArtwork = 512.f;

//other sizes
static CGFloat const iconSizeTabBarItem = 30.f;

static CGFloat const iconSize = iconSizeItunesArtwork; //the current size we're using
static CGFloat const pointScale = iconSizeIphone / iconSizeItunesArtwork; //the current size we're using

static CGFloat const r1 = 95.f;
static CGFloat const r2 = 134.f;
static CGFloat const r3 = 223.f;

static CGFloat const d1 = 33.f;  //c - r3
static CGFloat const d2 = 161.f; //c - r1
static CGFloat const c  = 256.f;
static CGFloat const d3 = 351.f; //c + r1
static CGFloat const d4 = 479.f; //c + r3

@interface BIGIcon()
@property (nonatomic) NSString *inputString;
@property (nonatomic) NSData *hashData;
@property (nonatomic) UIBezierPath *gridPath;
@property (nonatomic) UIBezierPath *maskPath;

@property (nonatomic) UIImage *previewImage;
@property (nonatomic) UIImage *artworkImage;
@property (nonatomic) UIImage *transparentArtworkImage;

@property (nonatomic) NSMutableArray *gridValues;
@end

@implementation BIGIcon

+ (instancetype)iconFromString:(NSString *)string showGrid:(BOOL)grid
{
    return [self iconFromString:string showGrid:grid gridAlpha:.4f];
}

+ (instancetype)iconFromString:(NSString *)string showGrid:(BOOL)grid gridAlpha:(CGFloat)gridAlpha
{
    BIGIcon *icon = [[self alloc] init];
    icon.inputString = string;
    icon.gridHidden = !grid;
    icon.gridAlpha = gridAlpha;
    
    NSData *sha256HashData = [[string dataUsingEncoding:NSUTF8StringEncoding] sha256Value];
    icon.hashData = sha256HashData;
    return icon;
}

+ (instancetype)glyphIconFromString:(NSString *)string
{
    BIGIcon *icon = [[self alloc] init];
    icon.inputString = string;
    icon.gridHidden = YES;
    icon.gridAlpha = 0.f;
    
    NSData *sha256HashData = [[string dataUsingEncoding:NSUTF8StringEncoding] sha256Value];
    icon.hashData = sha256HashData;
    return icon;
}

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        _gridValues = [NSMutableArray array];
    }
    return self;
}

- (UIImage *)previewImage
{
    if(!_previewImage)
        _previewImage = [self drawImageWithScale:(pointScale * iconScale) mask:YES];
    return _previewImage;
}

- (UIImage *)artworkImage
{
    if(!_artworkImage)
        _artworkImage = [self drawImageWithScale:iconScale mask:YES];
    return _artworkImage;
}

- (UIImage *)transparentArtworkImage
{
    if(!_transparentArtworkImage)
        _transparentArtworkImage = [self drawImageWithScale:iconScale mask:NO transparentBackground:YES];
    return _transparentArtworkImage;
}

- (UIImage *)glyphImage
{
    UIImage *glyph = [self drawImageWithScale:iconScale mask:NO transparentBackground:YES asGlyph:YES];
    UIImage *result = [glyph resizedImage:CGSizeMake(iconSizeTabBarItem, iconSizeTabBarItem) interpolationQuality:kCGInterpolationHigh];
    return result;
}

- (UIImage *)imageWithSize:(CGFloat)size mask:(BOOL)mask
{
    UIImage *baseImage = [self artworkImage];
    return [self resizeImage:baseImage toSize:floorf((iconSizeItunesArtwork / size)  * iconSizeItunesArtwork) scale:2];
}

# pragma mark - image

- (UIImage *)drawImageWithScale:(CGFloat)scale mask:(BOOL)mask
{
    return [self drawImageWithScale:scale mask:mask transparentBackground:NO];
}

- (UIImage *)drawImageWithScale:(CGFloat)scale mask:(BOOL)mask transparentBackground:(BOOL)transparent
{
    return [self drawImageWithScale:scale mask:mask transparentBackground:transparent asGlyph:NO];
}

- (UIImage *)drawImageWithScale:(CGFloat)scale mask:(BOOL)mask transparentBackground:(BOOL)transparent asGlyph:(BOOL)glyph
{
    const char *hashData = [self.hashData bytes];
    
    //get the foreground, background, and stroke colors
    int pos = 0;
    BOOL useLightBackground = hashData[pos++] % 2 == 1;
    BOOL fillOuterCircle = hashData[pos++] % 2 == 1;
    BOOL strokeOuterCircle = (hashData[pos++] % 2 == 1) && fillOuterCircle;
    
    UIColor *strokeColor = useLightBackground ? [UIColor blackColor] : [UIColor whiteColor];
    UIColor *foregroundColor = nil;
    UIColor *backgroundColor = nil;
    UIColor *circleColor = nil;
    if(strokeOuterCircle)
    {
        foregroundColor = useLightBackground ? [UIColor lightColorForSeed:hashData[pos++]] : [UIColor darkColorForSeed:hashData[pos++]];
        backgroundColor = [UIColor colorForSeed:hashData[pos++]];
        circleColor = [UIColor clearColor];
        pos++; //for circle color, to keep the branch in parody
    }
    else
    {
        foregroundColor = [UIColor colorForSeed:hashData[pos++]];
        backgroundColor = useLightBackground ? [UIColor lightColorForSeed:hashData[pos++]] : [UIColor darkColorForSeed:hashData[pos++]];
        circleColor = [UIColor colorForSeed:hashData[pos++]];
    }
    
    if(transparent)
        backgroundColor = [UIColor clearColor];
    
    if([circleColor isEqual:foregroundColor])
        circleColor = [UIColor colorForSeed:hashData[pos] + 1];
    
    //if we're a glyph and the circle if filled in, make the foreground color transparent.
    if(glyph)
        circleColor = [UIColor clearColor];
    
    UIColor *circleStrokeColor = foregroundColor;
    
    //begin the grid
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(iconSize, iconSize), NO, scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIBezierPath *grid = nil;
    if(mask)
        grid = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, iconSize, iconSize) byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(116.f, 116.f)];
    else
        grid = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, iconSize, iconSize)];

    [grid addClip]; //clips to the grids bounds
    if(!self.gridHidden)
        [grid strokeWithBlendMode:kCGBlendModeNormal alpha:1.f];
    CGContextSetLineWidth(context, 5.f);
    CGContextSetStrokeColorWithColor(context, strokeColor.CGColor);
    fillPathWithColorInContext(grid, backgroundColor, context);
    
    CGContextSetLineWidth(context, 2.f);
    CGContextSetStrokeColorWithColor(context, strokeColor.CGColor);
    
    CGContextSaveGState(context);
    
    //draw the outtermost circle first
    UIBezierPath *c3 = [UIBezierPath bezierPathWithArcCenter:pt(c,c) radius:r3 startAngle:-M_PI endAngle:M_PI clockwise:YES];
    
    if(fillOuterCircle) //fill in the circle
    {
        if(strokeOuterCircle)
        {
            CGContextSaveGState(context);
            CGContextSetStrokeColorWithColor(context, circleStrokeColor.CGColor);
            c3.lineWidth = 14.f;
            [c3 stroke];
            CGContextRestoreGState(context);
        }
        else
            fillPathWithColorInContext(c3, circleColor, context);
    }
    [grid appendPath:c3];
    
    //generate a grid based on the hash data, fill with the foreground color
    CGFloat gso = floorf((d1 * iconScale) * (fillOuterCircle ? 1.8f : 1.2f)); //grid start offset
    NSInteger totalSize = ((iconSize - (gso * 2)) * iconScale); //(60 percent);
    long gridSizeSeed = hashData[pos++];
    
    BOOL rowMirroring = (hashData[pos++] % 2 == 0);
    NSInteger gridSize = rowMirroring ? MAX(gridSizeSeed % 3, 0) + 4.0 : 5; //vary the grid size from 3-9
    NSInteger middleColumn = floorf(gridSize / 2.f);
    CGFloat gridDelta = floorf(totalSize / (gridSize * iconScale));
    
    NSInteger gridSpacing = ((hashData[pos++] % 2 == 0) ? 1.f : 0.f);
    
    NSAssert(pos <= 10, @"The byte offset must be less than ten going into the grid generation");
    pos = 10;
    
    for(int rX = 0; rX < gridSize; rX++)
    {
        for(int cX = 0; cX < gridSize; cX++)
        {
            if(rowMirroring && middleColumn < cX)
                break;
            BOOL isMiddleColmn = cX >= ceil(gridSize / 2);
            
            unsigned char gridValue = hashData[pos++];
            
            CGRect frame = CGRectMake((cX * gridDelta) + gso + (gridSpacing * gridSize/2), (rX * gridDelta) + gso, gridDelta - gridSpacing, gridDelta - gridSpacing);
            
            CGFloat mirrorXPos = ((gridSize - cX-1) * gridDelta) + gso;
            CGRect mirrorFrame = CGRectIntegral(CGRectMake(mirrorXPos, frame.origin.y, frame.size.width, frame.size.height));
            
            BOOL fillGrid = (gridValue % 2 == 1);
            if(fillGrid)
            {
                UIColor *fillColor = foregroundColor;
                fillRectangleWithColorInContext(frame, fillColor, context);
                
                if(!isMiddleColmn)
                    fillRectangleWithColorInContext(mirrorFrame, fillColor, context);
            }
        }
    }
    
    //draw the other circles
    UIBezierPath *c2 = [UIBezierPath bezierPath];
    addCircleWithCenterAndRadius(c2, pt(c,c), r2);
    [grid appendPath:c2];
    
    UIBezierPath *c1 = [UIBezierPath bezierPath];
    addCircleWithCenterAndRadius(c1, pt(c,c), r1);
    [grid appendPath:c1];
    
    //remove the clipping mask
    CGContextRestoreGState(context);
    
    if(!self.gridHidden)
    {
        CGContextSetStrokeColorWithColor(context, useLightBackground ? [UIColor darkTextColor].CGColor : [UIColor lightTextColor].CGColor);
        
        //diagonal lines
        addLineFromStartPointToPoint(grid, pt(0,0),   pt(iconSize,iconSize));
        addLineFromStartPointToPoint(grid, pt(0,iconSize), pt(iconSize,0));
        
        //horizontal lines
        addLineFromStartPointToPoint(grid, pt(0,d1), pt(iconSize,d1));
        addLineFromStartPointToPoint(grid, pt(0,d2), pt(iconSize,d2));
        addLineFromStartPointToPoint(grid, pt(0,c),  pt(iconSize,c));
        addLineFromStartPointToPoint(grid, pt(0,d3), pt(iconSize,d3));
        addLineFromStartPointToPoint(grid, pt(0,d4), pt(iconSize,d4));
        
        //vertical lines
        addLineFromStartPointToPoint(grid, pt(d1,0), pt(d1,iconSize));
        addLineFromStartPointToPoint(grid, pt(d2,0), pt(d2,iconSize));
        addLineFromStartPointToPoint(grid, pt(c,0),  pt(c, iconSize));
        addLineFromStartPointToPoint(grid, pt(d3,0), pt(d3,iconSize));
        addLineFromStartPointToPoint(grid, pt(d4,0), pt(d4,iconSize));
        
        [grid moveToPoint:pt(0,0)];
        
        [grid strokeWithBlendMode:kCGBlendModeNormal alpha:useLightBackground ? self.gridAlpha - .1 : self.gridAlpha];
    }
    else
        grid.lineWidth = 0.f;
    
    
    [grid closePath];
    
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return resultImage;
}


- (UIImage *)drawGlyphImageWithMask:(BOOL)mask transparentBackground:(BOOL)transparent asGlyph:(BOOL)glyph
{
    const char *hashData = [self.hashData bytes];
    
    //get the foreground, background, and stroke colors
    int pos = 0;
    BOOL useLightBackground = hashData[pos++] % 2 == 1;
    BOOL fillOuterCircle = hashData[pos++] % 2 == 1;
    BOOL strokeOuterCircle = (hashData[pos++] % 2 == 1) && fillOuterCircle;
    
    UIColor *strokeColor = useLightBackground ? [UIColor blackColor] : [UIColor whiteColor];
    UIColor *foregroundColor = nil;
    UIColor *backgroundColor = nil;
    UIColor *circleColor = nil;
    if(strokeOuterCircle)
    {
        foregroundColor = useLightBackground ? [UIColor lightColorForSeed:hashData[pos++]] : [UIColor darkColorForSeed:hashData[pos++]];
        backgroundColor = [UIColor colorForSeed:hashData[pos++]];
        circleColor = [UIColor clearColor];
        pos++; //for circle color, to keep the branch in parody
    }
    else
    {
        foregroundColor = [UIColor colorForSeed:hashData[pos++]];
        backgroundColor = useLightBackground ? [UIColor lightColorForSeed:hashData[pos++]] : [UIColor darkColorForSeed:hashData[pos++]];
        circleColor = [UIColor colorForSeed:hashData[pos++]];
    }
    
    if(transparent)
        backgroundColor = [UIColor clearColor];
    
    if([circleColor isEqual:foregroundColor])
        circleColor = [UIColor colorForSeed:hashData[pos] + 1];
    
    //if we're a glyph and the circle if filled in, make the foreground color transparent.
    if(glyph)
        circleColor = [UIColor clearColor];
    
    UIColor *circleStrokeColor = foregroundColor;
    
    //begin the grid
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(iconSize, iconSize), NO, 2.f);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    UIBezierPath *grid = nil;
    if(mask)
        grid = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, iconSize, iconSize) byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(116.f, 116.f)];
    else
        grid = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, iconSize, iconSize)];
    
    [grid addClip]; //clips to the grids bounds
    if(!self.gridHidden)
        [grid strokeWithBlendMode:kCGBlendModeNormal alpha:1.f];
    CGContextSetLineWidth(context, 5.f);
    CGContextSetStrokeColorWithColor(context, strokeColor.CGColor);
    fillPathWithColorInContext(grid, backgroundColor, context);
    
    CGContextSetLineWidth(context, 2.f);
    CGContextSetStrokeColorWithColor(context, strokeColor.CGColor);
    
    CGContextSaveGState(context);
    
    //draw the outtermost circle first
    UIBezierPath *c3 = [UIBezierPath bezierPathWithArcCenter:pt(c,c) radius:r3 startAngle:-M_PI endAngle:M_PI clockwise:YES];
    
    if(fillOuterCircle) //fill in the circle
    {
        if(strokeOuterCircle)
        {
            CGContextSaveGState(context);
            CGContextSetStrokeColorWithColor(context, circleStrokeColor.CGColor);
            c3.lineWidth = 14.f;
            [c3 stroke];
            CGContextRestoreGState(context);
        }
        else
            fillPathWithColorInContext(c3, circleColor, context);
    }
    [grid appendPath:c3];
    
    //generate a grid based on the hash data, fill with the foreground color
    CGFloat gso = floorf((d1 * iconScale) * (fillOuterCircle ? 1.8f : 1.2f)); //grid start offset
    NSInteger totalSize = ((iconSize - (gso * 2)) * iconScale); //(60 percent);
    long gridSizeSeed = hashData[pos++];
    
    BOOL rowMirroring = (hashData[pos++] % 2 == 0);
    NSInteger gridSize = rowMirroring ? MAX(gridSizeSeed % 3, 0) + 4.0 : 5; //vary the grid size from 3-9
    NSInteger middleColumn = floorf(gridSize / 2.f);
    CGFloat gridDelta = floorf(totalSize / (gridSize * iconScale));
    
    NSInteger gridSpacing = ((hashData[pos++] % 2 == 0) ? 1.f : 0.f);
    
    NSAssert(pos <= 10, @"The byte offset must be less than ten going into the grid generation");
    pos = 10;
    
    for(int rX = 0; rX < gridSize; rX++)
    {
        for(int cX = 0; cX < gridSize; cX++)
        {
            if(rowMirroring && middleColumn < cX)
                break;
            BOOL isMiddleColmn = cX >= ceil(gridSize / 2);
            
            unsigned char gridValue = hashData[pos++];
            
            CGRect frame = CGRectMake((cX * gridDelta) + gso + (gridSpacing * gridSize/2), (rX * gridDelta) + gso, gridDelta - gridSpacing, gridDelta - gridSpacing);
            
            CGFloat mirrorXPos = ((gridSize - cX-1) * gridDelta) + gso;
            CGRect mirrorFrame = CGRectIntegral(CGRectMake(mirrorXPos, frame.origin.y, frame.size.width, frame.size.height));
            
            BOOL fillGrid = (gridValue % 2 == 1);
            if(fillGrid)
            {
                UIColor *fillColor = foregroundColor;
                fillRectangleWithColorInContext(frame, fillColor, context);
                
                if(!isMiddleColmn)
                    fillRectangleWithColorInContext(mirrorFrame, fillColor, context);
            }
        }
    }
    
    //draw the other circles
    UIBezierPath *c2 = [UIBezierPath bezierPath];
    addCircleWithCenterAndRadius(c2, pt(c,c), r2);
    [grid appendPath:c2];
    
    UIBezierPath *c1 = [UIBezierPath bezierPath];
    addCircleWithCenterAndRadius(c1, pt(c,c), r1);
    [grid appendPath:c1];
    
    //remove the clipping mask
    CGContextRestoreGState(context);
    
    if(!self.gridHidden)
    {
        CGContextSetStrokeColorWithColor(context, useLightBackground ? [UIColor darkTextColor].CGColor : [UIColor lightTextColor].CGColor);
        
        //diagonal lines
        addLineFromStartPointToPoint(grid, pt(0,0),   pt(iconSize,iconSize));
        addLineFromStartPointToPoint(grid, pt(0,iconSize), pt(iconSize,0));
        
        //horizontal lines
        addLineFromStartPointToPoint(grid, pt(0,d1), pt(iconSize,d1));
        addLineFromStartPointToPoint(grid, pt(0,d2), pt(iconSize,d2));
        addLineFromStartPointToPoint(grid, pt(0,c),  pt(iconSize,c));
        addLineFromStartPointToPoint(grid, pt(0,d3), pt(iconSize,d3));
        addLineFromStartPointToPoint(grid, pt(0,d4), pt(iconSize,d4));
        
        //vertical lines
        addLineFromStartPointToPoint(grid, pt(d1,0), pt(d1,iconSize));
        addLineFromStartPointToPoint(grid, pt(d2,0), pt(d2,iconSize));
        addLineFromStartPointToPoint(grid, pt(c,0),  pt(c, iconSize));
        addLineFromStartPointToPoint(grid, pt(d3,0), pt(d3,iconSize));
        addLineFromStartPointToPoint(grid, pt(d4,0), pt(d4,iconSize));
        
        [grid moveToPoint:pt(0,0)];
        
        [grid strokeWithBlendMode:kCGBlendModeNormal alpha:useLightBackground ? self.gridAlpha - .1 : self.gridAlpha];
    }
    else
        grid.lineWidth = 0.f;
    
    
    [grid closePath];
    
    CGContextScaleCTM(context, iconSizeTabBarItem / iconSize, iconSizeTabBarItem / iconSize);
    
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return resultImage;
}

- (UIImage *)resizeImage:(UIImage *)image toSize:(CGFloat)size scale:(CGFloat)scale
{
    UIImage *result = [image resizedImage:CGSizeMake(size, size) interpolationQuality:kCGInterpolationHigh];
    return result;
}

# pragma mark - Write

- (BOOL)writeImage:(UIImage *)image withName:(NSString *)filename toXcassetFolder:(NSString *)xcassetFolder
{
    NSString *fullPath = [[xcassetFolder stringByAppendingPathComponent:filename] stringByAppendingPathExtension:@"png"];

    NSData *pngData = UIImagePNGRepresentation(image);
    NSError *error = nil;
    BOOL result = [pngData writeToFile:fullPath options:NSDataWritingFileProtectionCompleteUnlessOpen error:&error];
    return result;
}

- (void)writeIconBundleToDisk
{
    NSString *basePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *bundleName = [[[self hashString] substringToIndex:7] stringByAppendingPathExtension:@"xcassets"];
    NSString *bundlePath = [basePath stringByAppendingPathComponent:bundleName];
    
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:bundlePath error:&error]; //lets replace the folder
    if(error)
        error = nil; //file didn't exist
    
    NSString *iconPath = [bundlePath stringByAppendingPathComponent:@"BetaAppIcon.appiconset"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:iconPath withIntermediateDirectories:YES attributes:nil error:&error];
    NSAssert(!error, @"error on createDirectoryAtPath, why");
    
    NSString *artworkPath = [NSString pathWithComponents:@[basePath, bundleName, @"BetaItunesArtwork.imageset"]];
    [[NSFileManager defaultManager] createDirectoryAtPath:artworkPath withIntermediateDirectories:YES attributes:nil error:&error];
    NSAssert(!error, @"error on createDirectoryAtPath, why");
    
    UIImage *baseImage =   [self drawImageWithScale:1 mask:NO];
    UIImage *x2BaseImage = [self drawImageWithScale:2 mask:NO];
    UIImage *x3BaseImage = [self drawImageWithScale:3 mask:NO];
    
    [self writeImage:[self resizeImage:baseImage   toSize:iconSizeSpotlight scale:1] withName:@"40x40"    toXcassetFolder:iconPath];
    [self writeImage:[self resizeImage:x2BaseImage toSize:iconSizeSpotlight scale:2] withName:@"40x40@2x" toXcassetFolder:iconPath];
    [self writeImage:[self resizeImage:x3BaseImage toSize:iconSizeSpotlight scale:3] withName:@"40x40@3x" toXcassetFolder:iconPath];
    
    [self writeImage:[self resizeImage:baseImage   toSize:iconSizeSettings scale:1] withName:@"29x29"    toXcassetFolder:iconPath];
    [self writeImage:[self resizeImage:x2BaseImage toSize:iconSizeSettings scale:2] withName:@"29x29@2x" toXcassetFolder:iconPath];
    [self writeImage:[self resizeImage:x3BaseImage toSize:iconSizeSettings scale:3] withName:@"29x29@3x" toXcassetFolder:iconPath];
    
    [self writeImage:[self resizeImage:baseImage   toSize:iconSizeIpad scale:1] withName:@"76x76" toXcassetFolder:iconPath];
    [self writeImage:[self resizeImage:x2BaseImage toSize:iconSizeIpad scale:2] withName:@"76x76@2x" toXcassetFolder:iconPath];
    
    [self writeImage:[self resizeImage:x2BaseImage toSize:iconSizeIphone scale:2] withName:@"60x60@2x" toXcassetFolder:iconPath]; //has no time for non-retina
    [self writeImage:[self resizeImage:x3BaseImage toSize:iconSizeIphone scale:3] withName:@"60x60@3x" toXcassetFolder:iconPath];
    
    
    //iOS 5 & 6
//    [self writeImage:[self resizeImage:baseImage toSize:57.0 scale:1] withName:@"57x57-legacy" toXcassetFolder:iconPath];
//    [self writeImage:[self resizeImage:x2BaseImage toSize:57.0 scale:2] withName:@"57x57-legacy@2x" toXcassetFolder:iconPath];
    
    [self writeImage:[self resizeImage:baseImage toSize:50.0 scale:1] withName:@"50x50-legacy" toXcassetFolder:iconPath];
    [self writeImage:[self resizeImage:x2BaseImage toSize:50.0 scale:2] withName:@"50x50-legacy@2x" toXcassetFolder:iconPath];
    
    [self writeImage:[self resizeImage:baseImage toSize:72.0 scale:1] withName:@"72x72-legacy" toXcassetFolder:iconPath];
    [self writeImage:[self resizeImage:x2BaseImage toSize:72.0 scale:2] withName:@"72x72-legacy@2x" toXcassetFolder:iconPath];
    
    NSString *contentsIconPath = [[NSBundle mainBundle] pathForResource:@"BetaIconContents" ofType:@"json"];
    NSString *newIconPath = [iconPath stringByAppendingPathComponent:@"Contents.json"];
    [[NSFileManager defaultManager] copyItemAtPath:contentsIconPath toPath:newIconPath error:&error];
    NSAssert(!error, @"error on createDirectoryAtPath, why");
    
    [self writeImage:baseImage   withName:@"iTunesArtwork"    toXcassetFolder:artworkPath];
    [self writeImage:x2BaseImage withName:@"iTunesArtwork@2x" toXcassetFolder:artworkPath];
    
    NSString *contentsArtworkPath = [[NSBundle mainBundle] pathForResource:@"BetaItunesArtwork" ofType:@"json"];
    NSString *newArtworkPath = [artworkPath stringByAppendingPathComponent:@"Contents.json"];
    [[NSFileManager defaultManager] copyItemAtPath:contentsArtworkPath toPath:newArtworkPath error:&error];
    NSAssert(!error, @"error on createDirectoryAtPath, why");
    
    NSLog(@"Wrote asset bundle %@ to %@", bundleName, bundlePath);
}

# pragma mark - Hash

- (NSString *)hashString
{
    return [[self.inputString dataUsingEncoding:NSUTF8StringEncoding] sha256HashString];
}

//# pragma mark - TEST
//
//+ (BOOL)testOutputWithInput:(NSString *)inputString
//{
//    NSData *sha256HashData = [[inputString dataUsingEncoding:NSUTF8StringEncoding] sha256Value];
//    NSLog(@"%@", [sha256HashData debugDescription]);
//    
//    const char *bytes = [sha256HashData bytes];
//    
//    int pos = 0;
//    NSLog(@"%02x", bytes[pos]);
//    
//    BOOL useLightBackground = bytes[pos++] % 2 == 1;
//    NSLog(@"%02x", bytes[pos]);
//    NSAssert(useLightBackground == NO, @"useLightBackground");
//    
//    BOOL fillOuterCircle = bytes[pos++] % 2 == 1;
//    NSLog(@"%02x", bytes[pos]);
//    NSAssert(fillOuterCircle == NO, @"fillOuterCircle");
//    
//    BOOL strokeOuterCircle = (bytes[pos++] % 2 == 1) && fillOuterCircle;
//    NSLog(@"%02x", bytes[pos]);
//    NSAssert(strokeOuterCircle == NO, @"strokeOuterCircle");
//    
//    BOOL foregroundColor = [[UIColor colorForSeed:bytes[pos++]] isEqual:[UIColor colorForSeed:'\x99']];
//    NSLog(@"%02x", bytes[pos]);
//    NSAssert(foregroundColor, @"foregroundColor");
//    
//    BOOL backgroundColor = [[UIColor darkColorForSeed:bytes[pos++]] isEqual:[UIColor darkColorForSeed:(char)'0x6d']];
//    NSLog(@"%02x", bytes[pos]);
//    NSAssert(backgroundColor, @"backgroundColor");
//    
//    long gridSizeSeed = bytes[pos++];
//    BOOL gridSeed = [@(gridSizeSeed) compare: @('\x38')];
//    NSLog(@"%02x", bytes[pos]);
//    NSAssert(gridSeed, @"gridSeed");
//    
//    BOOL rowMirroring = bytes[pos++] % 2 == 0;
//    NSLog(@"%02x", bytes[pos]);
//    NSAssert(rowMirroring == YES, @"rowMirroring");
//    
//    BOOL gridSpacing = bytes[pos++] % 2 == 0;
//    NSLog(@"%02x", bytes[pos]);
//    NSAssert(gridSpacing == YES, @"gridSpacing");
//    
//    NSAssert(pos <= 10, @"The byte offset must be less than ten going into the grid generation");
//    pos = 10;
//    
//    NSInteger gridSize = rowMirroring ? MAX(gridSizeSeed % 3, 0) + 4.0 : 5; //vary the grid size from 3-9
//    NSInteger middleColumn = floorf(gridSize / 2.f);
//    
//    for(int rX = 0; rX < gridSize; rX++)
//    {
//        for(int cX = 0; cX < gridSize; cX++)
//        {
//            int value = bytes[pos++];
//            if(rowMirroring && middleColumn <= cX)
//                break;
//            
//            BOOL fillGrid = value % 2 == 0;
//            if(fillGrid)
//                NSLog(@"%02x", bytes[pos]);
//        }
//    }
//    return YES;
//}

@end
