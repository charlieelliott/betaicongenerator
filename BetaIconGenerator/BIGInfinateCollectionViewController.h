//
//  BIGInfinateCollectionViewController.h
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 10/30/13.
//  Copyright (c) 2013 Hidden World Hut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIGInfinateCollectionViewController : UICollectionViewController

@end
