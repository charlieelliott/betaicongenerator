//
//  BIGInfinateCollectionViewController.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 10/30/13.
//  Copyright (c) 2013 Hidden World Hut. All rights reserved.
//

#import "BIGInfinateCollectionViewController.h"
#import "BIGInfinateCollectionViewIconCell.h"
#import "BIGIcon.h"

@interface BIGInfinateCollectionViewController () <UICollectionViewDelegateFlowLayout>
@property (nonatomic) NSMutableArray *icons;

@property (nonatomic) BIGIcon *selectedIcon;
@end

@implementation BIGInfinateCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.icons = [NSMutableArray array];
    
    NSString *bundleName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"];
//    [BIGIcon testOutputWithInput:bundleName];
    
    BIGIcon *appIcon = [BIGIcon iconFromString:bundleName showGrid:YES];
    [self.icons addObject:appIcon];
    
    // generated icon
    [self.icons addObject:[BIGIcon iconFromString:@"celliott0465" showGrid:NO]];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

# pragma mark - CollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.icons count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BIGInfinateCollectionViewIconCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"iconCell" forIndexPath:indexPath];
    [cell updateWithIcon:self.icons[indexPath.item]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.icons[indexPath.item] == self.selectedIcon)
        [self.selectedIcon writeIconBundleToDisk];
    
    self.selectedIcon = self.icons[indexPath.item];
    
    [collectionView reloadItemsAtIndexPaths:@[indexPath]];
    [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([[collectionView indexPathsForSelectedItems] containsObject:indexPath])
        return CGSizeMake(256.f, 256.f);
    else
        return CGSizeMake(60.f, 60.f);
}

@end
