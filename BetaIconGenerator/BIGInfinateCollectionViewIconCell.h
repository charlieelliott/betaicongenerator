//
//  BIGInfinateCollectionViewIconCell.h
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 10/30/13.
//  Copyright (c) 2013 Hidden World Hut. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIGIcon.h"

@interface BIGInfinateCollectionViewIconCell : UICollectionViewCell

@property (nonatomic, readonly) BIGIcon *icon;

- (void)updateWithIcon:(BIGIcon *)icon;

@end
