//
//  BIGInfinateCollectionViewIconCell.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 10/30/13.
//  Copyright (c) 2013 Hidden World Hut. All rights reserved.
//

#import "BIGInfinateCollectionViewIconCell.h"

@interface BIGInfinateCollectionViewIconCell()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic) BIGIcon *icon;
@end

@implementation BIGInfinateCollectionViewIconCell

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.imageView.image = nil;
    self.icon = nil;
}

- (void)updateWithIcon:(BIGIcon *)icon
{
    self.icon = icon;
    self.imageView.image = [icon imageWithSize:self.frame.size.width mask:YES];
}

@end
