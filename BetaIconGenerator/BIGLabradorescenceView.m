//
//  BIGLabradorescenceView.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 10/14/13.
//  Copyright (c) 2013 Hidden World Hut. All rights reserved.
//

#import "BIGLabradorescenceView.h"

@interface BIGLabradorescenceView()
@end

@implementation BIGLabradorescenceView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self buildTriclinicUnitCell];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self buildTriclinicUnitCell];
    }
    return self;
}

- (void)buildTriclinicUnitCell
{
    self.layer.backgroundColor = [UIColor clearColor].CGColor;
//    CATransform3D sublt = CATransform3DIdentity;
//    sublt.m34 = -500;
//    self.layer.sublayerTransform = sublt;
    for(int i = 0; i < 10; i++)
    {
        CAShapeLayer *front = [self generateShapeLayer];
        front.zPosition = 10 - i;
        [self.layer addSublayer:front];
    }
//    CAShapeLayer *front = [self generateShapeLayer];
//    [self.layer addSublayer:front];
//    
//    CAShapeLayer *left = [self generateShapeLayer];
//    
//    transform = CATransform3DTranslate(transform, self.layer.bounds.size.width / 2.f, 0, 0);
//    left.transform = transform;
//    [self.layer addSublayer:left];
}

- (void)layoutSublayersOfLayer:(CALayer *)layer
{
    [super layoutSublayersOfLayer:layer];
    
    for(CAShapeLayer *subl in layer.sublayers)
    {
        NSInteger zi = 10 - subl.zPosition;
        CATransform3D transform = CATransform3DMakeRotation(M_PI / 180 * (zi * 1.2), 1.f, 0, .1f);
        transform = CATransform3DTranslate(transform, -zi * 5.f, 0, 0);
        transform.m34 = -1 / 500;
        subl.transform = transform;
    }
}

- (CAShapeLayer *)generateShapeLayer
{
    CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
    shapeLayer.frame = self.layer.bounds;
    shapeLayer.borderColor = [UIColor greenColor].CGColor;
    shapeLayer.borderWidth = 2.f;
    shapeLayer.backgroundColor = [UIColor grayColor].CGColor;
    return shapeLayer;
}

- (IBAction)transformSliderChangedValue:(UISlider *)sender
{
    self.layer.sublayerTransform = CATransform3DMakeTranslation(5 * sender.value, 0, 0);
}

@end
