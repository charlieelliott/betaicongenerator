//
//  BIGLabradorescenceViewController.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 3/1/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import "BIGLabradorescenceViewController.h"
#import "BIGIcon.h"
#import "UIImage+ImageEffects.h"
@import CoreMotion;

NS_INLINE CGFloat CGFloatClampToValues(CGFloat value, CGFloat minimum, CGFloat maximum)
{
    return (value < minimum) ? minimum : ((value > maximum) ? maximum : value);
}

NS_INLINE CGVector CGVectorClamp(CGVector vector)
{
    vector.dx = CGFloatClampToValues(vector.dx, -1, 1);
    vector.dy = CGFloatClampToValues(vector.dy, -1, 1);
    return vector;
}

@interface BIGLabradorescenceViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *iconView1;
@property (weak, nonatomic) IBOutlet UIImageView *iconView2;
@property (weak, nonatomic) IBOutlet UISlider *movementSlider;

@property (nonatomic) BIGIcon *icon1;
@property (nonatomic) BIGIcon *icon2;

@property (nonatomic) CAGradientLayer *gradientX;
@property (nonatomic) CAGradientLayer *gradientY;
@property (nonatomic) CALayer *gradientMask;

@property (nonatomic) CMMotionManager *motionManager;

@end

@implementation BIGLabradorescenceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.icon1 = [BIGIcon iconFromString:@"icon1" showGrid:YES];
    self.icon2 = [BIGIcon iconFromString:@"icon1" showGrid:YES];
    
    self.iconView1.image = [self.icon1.artworkImage applyLightEffectWithBlurRadius:2.f];
    self.iconView2.image = self.icon2.artworkImage;
    
    self.gradientMask = [CALayer new];
    self.gradientMask.anchorPoint = CGPointZero;
    self.gradientMask.backgroundColor = [UIColor clearColor].CGColor;
    
    self.gradientX = [CAGradientLayer new];
    self.gradientX.anchorPoint = CGPointZero;
    self.gradientX.backgroundColor = [UIColor clearColor].CGColor;
    
    self.gradientY = [CAGradientLayer new];
    self.gradientY.anchorPoint = CGPointZero;
    self.gradientY.backgroundColor = [UIColor clearColor].CGColor;
    
    [self.gradientMask addSublayer:self.gradientX];
    [self.gradientMask addSublayer:self.gradientY];
    
    self.iconView1.layer.mask = self.gradientMask;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue]
                                            withHandler:^ (CMDeviceMotion *devMotion, NSError *error)
     {
         CMAcceleration gravity = devMotion.gravity;
         CGFloat span = 1.f;
         CGVector gravityVector = CGVectorClamp(CGVectorMake(gravity.x, -gravity.y));
         self.gradientX.startPoint = CGPointMake(MAX(gravityVector.dx - .2, -1) + 1, MAX(gravityVector.dy - .2, -1) + 1);
         self.gradientX.endPoint = CGPointMake(MIN(gravityVector.dx + .2, 1), MIN(gravityVector.dy + .2, 1));
         self.gradientX.locations = @[@0, @(.5 - (span/2)), @(.5 - (span/4)), @.5, @(.5 + (span/4)), @(.5 + (span/2)), @1];
         
//         self.gradientY.startPoint = CGPointMake(0, MAX(gravityVector.dy - .2, -1) + 1);
//         self.gradientY.endPoint = CGPointMake(0, MIN(gravityVector.dy + .2, 1));
//         self.gradientY.locations = @[@0, @(.5 - (span/2)), @(.5 - (span/4)), @.5, @(.5 + (span/4)), @(.5 + (span/2)), @1];
         
         self.iconView1.alpha = fabs(1 - gravityVector.dy);
         
         CGFloat baseAlpha = 0.f;
         self.gradientX.colors = @[(id)[UIColor colorWithWhite:0 alpha:baseAlpha].CGColor,
                                   (id)[UIColor colorWithWhite:0 alpha:baseAlpha].CGColor,
                                   (id)[UIColor colorWithWhite:0 alpha:baseAlpha + .2f].CGColor,
                                   (id)[UIColor colorWithWhite:0 alpha:baseAlpha + .3f].CGColor,
                                   (id)[UIColor colorWithWhite:0 alpha:baseAlpha + .2f].CGColor,
                                   (id)[UIColor colorWithWhite:0 alpha:baseAlpha].CGColor,
                                   (id)[UIColor colorWithWhite:0 alpha:baseAlpha].CGColor,];
         
//         self.gradientY.colors = @[(id)[UIColor colorWithWhite:0 alpha:baseAlpha].CGColor,
//                                   (id)[UIColor colorWithWhite:0 alpha:baseAlpha].CGColor,
//                                   (id)[UIColor colorWithWhite:0 alpha:baseAlpha + .1f].CGColor,
//                                   (id)[UIColor colorWithWhite:0 alpha:baseAlpha + .3f].CGColor,
//                                   (id)[UIColor colorWithWhite:0 alpha:baseAlpha + .1f].CGColor,
//                                   (id)[UIColor colorWithWhite:0 alpha:baseAlpha].CGColor,
//                                   (id)[UIColor colorWithWhite:0 alpha:baseAlpha].CGColor,];
         
         self.gradientX.bounds = self.iconView1.bounds;
//         self.gradientY.bounds = self.iconView1.bounds;
         self.gradientMask.bounds = self.iconView1.bounds;
     }];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (CMMotionManager *)motionManager
{
    if(_motionManager)
        return _motionManager;
    
    _motionManager = [[CMMotionManager alloc] init];
    
    //    _motionManager.gyroUpdateInterval = 0.50;
    //    _motionManager.accelerometerUpdateInterval=0.50;
    _motionManager.deviceMotionUpdateInterval = .1f;
    
    return _motionManager;
}

@end
