//
//  BIGTabBarViewController.h
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 3/13/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIGTabBarViewController : UITabBarController

@end
