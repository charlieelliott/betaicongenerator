//
//  BIGTabBarViewController.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 3/13/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import "BIGTabBarViewController.h"
#import "BIGIcon.h"

@interface BIGTabBarViewController ()

@end

@implementation BIGTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    for(UITabBarItem *item in self.tabBar.items)
    {
        [item setImage:[BIGIcon glyphIconFromString:item.title].glyphImage];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
