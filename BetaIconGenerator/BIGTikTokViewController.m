//
//  BIGTikTokViewController.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 3/11/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import "BIGTikTokViewController.h"
#import "BIGIcon.h"

@interface BIGTikTokViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;
@property (nonatomic) NSTimer *timer;

@end

@implementation BIGTikTokViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    BIGIcon *now = [BIGIcon iconFromString:[@([[NSDate date] timeIntervalSince1970]) stringValue] showGrid:NO];
    self.mainImageView.image = now.artworkImage;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(tikTok) userInfo:nil repeats:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.timer invalidate];
    self.timer = nil;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)tikTok
{
    BIGIcon *now = [BIGIcon iconFromString:[@(floor([[NSDate date] timeIntervalSince1970] * 1000)) stringValue] showGrid:NO];
    self.mainImageView.image = now.artworkImage;
}

@end
