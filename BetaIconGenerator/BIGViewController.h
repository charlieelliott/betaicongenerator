//
//  BIGViewController.h
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 10/10/13.
//  Copyright (c) 2013 Hidden World Hut. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIGIcon.h"

@interface BIGViewController : UIViewController
@property (nonatomic) BIGIcon *icon;
@end
