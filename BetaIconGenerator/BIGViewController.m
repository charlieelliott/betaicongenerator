//
//  BIGViewController.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 10/10/13.
//  Copyright (c) 2013 Hidden World Hut. All rights reserved.
//

#import "BIGViewController.h"

@interface BIGViewController () <UIScrollViewDelegate>
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) IBOutlet UIScrollView *iconScrollView;

@property (nonatomic) CGFloat minScreenDelta;
@end

@implementation BIGViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.imageView = [[UIImageView alloc] initWithFrame:self.iconScrollView.frame];
    [self.iconScrollView addSubview:self.imageView];
    
    CGSize size = self.view.frame.size;
    self.minScreenDelta = MIN(size.width, size.height) - (8 * 2);
    self.iconScrollView.contentInset = UIEdgeInsetsMake(fabs(size.width - size.height)/2.f, 8, 0, 8);
    
    self.iconScrollView.backgroundColor = [UIColor colorWithRed:0.153 green:0.153 blue:0.153 alpha:1];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    self.icon = [BIGIcon iconFromString:[[NSUUID UUID] UUIDString] showGrid:NO];
//    UIImage *iconImage = self.icon.iconImages[@"iTunesArtwork@2x"];
//    self.imageView.image = iconImage;
//    CGFloat size = iconImage.size.width / iconImage.scale;
//    [self.iconScrollView setMinimumZoomScale:self.minScreenDelta/size];
//    [self.iconScrollView setMaximumZoomScale:(self.minScreenDelta/size) * 5.f];
//    self.imageView.frame = CGRectMake(0,0,size,size);
//    self.iconScrollView.contentSize = CGSizeMake(size,size);
//    self.iconScrollView.zoomScale = self.iconScrollView.minimumZoomScale;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - Scroll view delegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

@end
