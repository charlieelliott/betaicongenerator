// Playground - noun: a place where people can play

import UIKit

extension UIColor {
    convenience init(hex: CInt) {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        
        red   = CGFloat((hex & 0xFF0000) >> 16) / 255.0
        green = CGFloat((hex & 0x00FF00) >> 8)  / 255.0
        blue  = CGFloat( hex & 0x0000FF) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}

extension UIBezierPath {
    func appendLineWithStart(start: CGPoint, end: CGPoint)
    {
        self.moveToPoint(start)
        self.addLineToPoint(end)
        self.closePath()
    }
    
    func appendCircleWithCenter(center: CGPoint, radius: CGFloat)
    {
        self.moveToPoint(center)
        self.addArcWithCenter(center, radius: radius, startAngle: CGFloat(-M_PI), endAngle: CGFloat(M_PI), clockwise: true)
        self.closePath()
    }
}

let iconScale: CGFloat = 2.0;
let iconSizeSettings: CGFloat = 29.0;
let iconSizeSpotlight: CGFloat = 40.0;
let iconSizeIphone: CGFloat = 60.0;
let iconSizeIpad: CGFloat = 76.0;
let iconSizeItunesArtwork: CGFloat = 512.0;

//other sizes
let iconSizeTabBarItem = 30.0;

let iconSize: CGFloat = iconSizeItunesArtwork; //the current size we're using
let pointScale: CGFloat = iconSizeIphone / iconSizeItunesArtwork; //the current size we're using

let r1: CGFloat = 95.0;
let r2: CGFloat = 134.0;
let r3: CGFloat = 223.0;

let d1: CGFloat = 33.0;  //c - r3
let d2: CGFloat = 161.0; //c - r1
let c : CGFloat = 256.0;
let d3: CGFloat = 351.0; //c + r1
let d4: CGFloat = 479.0; //c + r3

func fillPathWithColorInContext(path: UIBezierPath, color: UIColor, context: CGContextRef)
{
    CGContextSetFillColorWithColor(context, color.CGColor);
    path .fillWithBlendMode(kCGBlendModeNormal, alpha: 1.0)
}

func addLineFromStartPointToPoint(path: UIBezierPath, start: CGPoint, end: CGPoint)
{
    path .moveToPoint(start)
    path .addLineToPoint(end)
    path .closePath()
}

func addCircleWithCenterAndRadius(path: UIBezierPath, center: CGPoint, radius: CGFloat)
{
    path .moveToPoint(center)
    path .addArcWithCenter(center, radius: radius, startAngle: CGFloat(-M_PI), endAngle: CGFloat(M_PI), clockwise: true)
    path .closePath()
}

func pt(x: CGFloat, y: CGFloat) -> CGPoint {
    return CGPointMake(x, y)
}

func createGridLines() -> UIBezierPath {
    
    //icon sized path
    var grid = UIBezierPath(rect: CGRect(x: 0, y: 0, width: iconSize, height: iconSize))
    
    //draw circles
    grid.appendCircleWithCenter(pt(c,c), radius: r3)
    grid.appendCircleWithCenter(pt(c,c), radius: r2)
    grid.appendCircleWithCenter(pt(c,c), radius: r1)
    
    //diagonal lines
    grid.appendLineWithStart(pt(0,0),        end: pt(iconSize,iconSize))
    grid.appendLineWithStart(pt(0,iconSize), end: pt(iconSize, 0))
    
    //horizontal lines
    grid.appendLineWithStart(pt(0,d1), end: pt(iconSize,d1))
    grid.appendLineWithStart(pt(0,d2), end: pt(iconSize,d2))
    grid.appendLineWithStart(pt(0,c),  end: pt(iconSize,c))
    grid.appendLineWithStart(pt(0,d3), end: pt(iconSize,d3))
    grid.appendLineWithStart(pt(0,d4), end: pt(iconSize,d4))
    
    //vertical lines
    grid.appendLineWithStart(pt(d1,0), end: pt(d1,iconSize))
    grid.appendLineWithStart(pt(d2,0), end: pt(d2,iconSize))
    grid.appendLineWithStart(pt(c, 0), end: pt(c, iconSize))
    grid.appendLineWithStart(pt(d3,0), end: pt(d3,iconSize))
    grid.appendLineWithStart(pt(d4,0), end: pt(d4,iconSize))
    
    return grid
}

func createGrid() {

    var pos = 0
    
    let fillOuterCircle : CGFloat = 1.8 //1.2 (false)
    //generate a grid based on the hash data, fill with the foreground color
    
    var gso: Float = floorf((Float)(d1 * iconScale * fillOuterCircle)) //grid start offset
    var totalSize: Float = ((Float(iconSize) - (gso * 2.0)) * Float(iconScale)); //(60 percent);
    let gridSizeSeed: Float = 6.0//hashData[pos++];
    
    let rowMirroring = true//(hashData[pos++] % 2 == 0);
    let gridSize: Float = rowMirroring ? max(gridSizeSeed % 3, 0) + 4.0 : 5; //vary the grid size from 3-9
    let middleColumn: Float = floorf(gridSize / 2);
    let gridDelta: Float = floorf(totalSize / (gridSize * Float(iconScale)));
    
    let gridSpacing: Float = 1.0//((hashData[pos++] % 2 == 0) ? 1.f : 0.f);
    
//    NSAssert(pos <= 10, @"The byte offset must be less than ten going into the grid generation");
    pos = 10;
    
    for var rX: Float = 0.0; rX < gridSize; rX++ {
        for var cX: Float = 0.0; cX < gridSize; cX++ {
        
            if(rowMirroring && middleColumn < cX) {
                break;
            }
            var isMiddleColmn = cX >= ceil(gridSize / 2);
            
            var gridValue : UnsignedFixed = 0x4//hashData[pos++];
            
            var frame = CGRectZero
            frame.origin.x = CGFloat((cX * gridDelta) + gso + (gridSpacing * gridSize / 2.0))
            frame.origin.y = CGFloat((rX * gridDelta) + gso)
            frame.size.width = CGFloat(gridDelta - gridSpacing)
            frame.size.height = CGFloat(gridDelta - gridSpacing)
            
            var mirrorXPos = ((gridSize - cX-1) * gridDelta) + gso;
            var mirrorFrame = CGRectIntegral(CGRectMake(CGFloat(mirrorXPos), frame.origin.y, frame.size.width, frame.size.height));
            
            var fillGrid = (gridValue % 2 == 1);
            if(fillGrid)
            {
                var fillColor = foregroundColor;
                fillRectangleWithColorInContext(frame, fillColor, context);
                
                if(!isMiddleColmn)
                fillRectangleWithColorInContext(mirrorFrame, fillColor, context);
            }
        }
    }
}

func drawBackground() {
    var strokeColor = UIColor .blackColor()//useLightBackground ? [UIColor blackColor] : [UIColor whiteColor];
    var backgroundColor = UIColor(hex: 0x2c3e50)
    var foregroundColor = UIColor(hex: 0x16a085)
    var circleStrokeColor = foregroundColor;
    
    var scale = 2.0
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(CGFloat(iconSize), CGFloat(iconSize)), false, CGFloat(scale))
    var context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 5.0);
    CGContextSetStrokeColorWithColor(context, UIColor.lightTextColor().CGColor)
    
    var grid = createGridLines()
    
    fillPathWithColorInContext(grid, backgroundColor, context)
    
    grid.addClip()
    
    grid.strokeWithBlendMode(kCGBlendModeClear, alpha: 1.0)
}

drawBackground()

