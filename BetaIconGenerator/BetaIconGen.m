//
//  BetaIconGen.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 3/26/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import "BIGIcon.h"

int main(int argc, const char * argv[])
{
	@autoreleasepool
	{
        [BIGIcon testOutputWithInput:@(argv[0])];
        BIGIcon *icon = [BIGIcon iconFromString:@(argv[0]) showGrid:(BOOL)argv[1]];
        [icon writeIconBundleToDisk];
	}
    return 0;
}