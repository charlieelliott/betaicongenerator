//
//  NSData+SHA256.h
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 2/17/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (SHA256)

- (NSData *)sha256Value;
- (NSString *)sha256HashString;

- (NSData *)randomData;

@end
