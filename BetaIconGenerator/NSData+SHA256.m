//
//  NSData+SHA256.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 2/17/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import "NSData+SHA256.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation NSData (SHA256)

- (NSData *)sha256Value
{
    unsigned char hash[CC_SHA256_DIGEST_LENGTH];
    unsigned char *result = CC_SHA256([self bytes], (CC_LONG)[self length], hash);
    
    unsigned int convertedHash[CC_SHA256_DIGEST_LENGTH];
    
    // Setup our Objective-C output.
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    
    // Parse through the CC_SHA256 results (stored inside of digest[]).
    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
        NSNumber *hashValue = @(hash[i]);
        convertedHash[i] = [hashValue unsignedIntValue];
        [output appendFormat:@"%02x", convertedHash[i]];
    }
    
    NSData *sha1 = nil;
    if(result)
        sha1 = [NSData dataWithBytes:hash length:CC_SHA256_DIGEST_LENGTH];
    
//    // Parse through the CC_SHA256 results (stored inside of digest[]).
//    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
//        [output appendFormat:@"%02x", hash[i]];
//    }
    
    NSLog(@"sha256 : %@", output);
    
    return sha1;// [output dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)sha256HashString
{
    unsigned char hash[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256([self bytes], (CC_LONG)[self length], hash);
    
    // Setup our Objective-C output.
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    
    // Parse through the CC_SHA256 results (stored inside of digest[]).
    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", hash[i]];
    }
    
    return [output copy];
}

- (NSData *)randomData
{
//    cc_
    return nil;
}

@end
