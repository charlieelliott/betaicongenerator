//
//  NSString+SHA256.h
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 2/17/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SHA256)

- (NSString *)sha256HashString;

@end
