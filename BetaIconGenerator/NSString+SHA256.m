//
//  NSString+SHA256.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 2/17/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import "NSString+SHA256.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation NSString (SHA256)

- (NSString *)sha256HashString
{
    NSData *stringData = [self dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char hash[CC_SHA256_DIGEST_LENGTH];
    unsigned char *result = CC_SHA256([stringData bytes], (CC_LONG)[stringData length], hash);
    
    if(result)
    {
        NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
        for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
            [output appendFormat:@"%02x", hash[i]];
        }
        
        return [output copy];
    }
    
    return nil;
}

@end
