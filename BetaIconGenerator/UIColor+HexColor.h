//
//  UIColor+HexColor.h
//  flappyFan
//
//  Created by Charlie Elliott on 2/12/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColor)

+ (UIColor *)colorWithHEX:(int32_t)hex;

+ (UIColor *)darkColorForSeed:(char)seed;
+ (UIColor *)lightColorForSeed:(char)seed;

+ (UIColor *)colorForSeed:(char)seed;

@end
