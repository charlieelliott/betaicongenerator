//
//  UIColor+HexColor.m
//  flappyFan
//
//  Created by Charlie Elliott on 2/12/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#import "UIColor+HexColor.h"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation UIColor (HexColor)

+ (UIColor *)colorWithHEX:(int32_t)hex
{
    return UIColorFromRGB(hex);
}

+ (UIColor *)colorForSeed:(char)seed
{
    return [UIColor big_colors][seed % [[UIColor big_colors] count]];
}

+ (UIColor *)lightColorForSeed:(char)seed
{
    return [UIColor big_lightColors][seed % [[UIColor big_lightColors] count]];
}

+ (UIColor *)darkColorForSeed:(char)seed
{
    return [UIColor big_darkColors][seed % [[UIColor big_darkColors] count]];
}

# pragma mark - Static Colors

+ (NSArray *)big_colors
{
    static NSArray *_colors = nil;
    if(_colors)
        return _colors;
    
//    _colors = @[
//                [UIColor colorWithHue:0.772 saturation:0.545 brightness:0.820 alpha:1.f],
//                [UIColor colorWithHue:0.066 saturation:0.747 brightness:0.992 alpha:1.f],
//                [UIColor colorWithHue:0.999 saturation:0.675 brightness:0.894 alpha:1.f],
//                [UIColor colorWithHue:0.554 saturation:0.695 brightness:0.875 alpha:1.f],
//                [UIColor colorWithHue:0.214 saturation:0.419 brightness:0.796 alpha:1.f]];
    _colors = @[
                [UIColor colorWithHEX:0x1abc9c], //flatUIColors
                [UIColor colorWithHEX:0x16a085], //flatUIColors
                [UIColor colorWithHEX:0xf1c40f], //flatUIColors
                [UIColor colorWithHEX:0xf39c12], //flatUIColors
                [UIColor colorWithHEX:0x2ecc71], //flatUIColors
                [UIColor colorWithHEX:0x27ae60], //flatUIColors
                [UIColor colorWithHEX:0xe67e22], //flatUIColors
                [UIColor colorWithHEX:0xd35400], //flatUIColors
                [UIColor colorWithHEX:0x3498db], //flatUIColors
                [UIColor colorWithHEX:0x2980b9], //flatUIColors
                [UIColor colorWithHEX:0xe74c3c], //flatUIColors
                [UIColor colorWithHEX:0xc0392b], //flatUIColors
                [UIColor colorWithHEX:0x9b59b6], //flatUIColors
                [UIColor colorWithHEX:0x8e44ad], //flatUIColors
                
                [UIColor colorWithHEX:0x9EBC55], //flushUI
                [UIColor colorWithHEX:0x3298C6], //flushUI
                [UIColor colorWithHEX:0xC63A3C], //flushUI
                [UIColor colorWithHEX:0xF17C31], //flushUI
                [UIColor colorWithHEX:0x8E57AE], //flushUI
                ];
    return _colors;
}

+ (NSArray *)big_darkColors
{
    static NSArray *_darkColors = nil;
    if(_darkColors)
        return _darkColors;
    
    _darkColors = @[
                    [UIColor colorWithHEX:0x34495e], //flatUIColors
                    [UIColor colorWithHEX:0x2c3e50], //flatUIColors
                    [UIColor colorWithHEX:0x7f8c8d], //flatUIColors
                    
                    [UIColor colorWithHEX:0x525659], //flushUI
                    ];
    
    return _darkColors;
}

+ (NSArray *)big_lightColors
{
    static NSArray *_lightColors = nil;
    if(_lightColors)
        return _lightColors;
    
    _lightColors = @[
                     [UIColor colorWithHEX:0xecf0f1], //flatUIColors
                     [UIColor colorWithHEX:0xbdc3c7], //flatUIColors
                     [UIColor colorWithHEX:0x95a5a6], //flatUIColors
                     
                     [UIColor colorWithHEX:0xD0D0D0], //flushUI
                     ];
    
    return _lightColors;
}

@end
