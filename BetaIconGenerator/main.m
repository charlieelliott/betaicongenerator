//
//  main.m
//  BetaIconGenerator
//
//  Created by Charlie Elliott on 10/10/13.
//  Copyright (c) 2013 Hidden World Hut. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BIGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BIGAppDelegate class]));
    }
}
